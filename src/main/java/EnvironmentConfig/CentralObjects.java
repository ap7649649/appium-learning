package EnvironmentConfig;
import Utilities.Commons;
import Utilities.WrapperFunctions;
import io.appium.java_client.android.AndroidDriver;
import com.aventstack.extentreports.ExtentTest;

public class CentralObjects {
    private static AndroidDriver driver;
	private static Commons objCommonsPage;
	private static WrapperFunctions objWrapperFunctionsPage;
	private static ExtentTest test;
    
    
    public void setAndroidDriver(AndroidDriver driver) {
		CentralObjects.driver = driver;
	}
	public static AndroidDriver getAndroidDriver() {
		return driver;
	}
	public void setObjExtentTest(ExtentTest objExtentTest) {
		this.test = objExtentTest;
	}
    public static ExtentTest getObjExtentTest() {
		return test;
	}

	public void setCommonsPage(Commons objCommons) {
		objCommonsPage = objCommons;
	}
	public static Commons getCommonsPage() {
		return objCommonsPage;
	}
	public void setWrapperFunctions(WrapperFunctions objWrapperFunctions) {
		objWrapperFunctionsPage = objWrapperFunctions;
	}
	public static WrapperFunctions getObjWrapperFunctions() {
		return objWrapperFunctionsPage;
	}

}