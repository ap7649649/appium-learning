package EnvironmentConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;
import java.util.Set;
import Utilities.Commons;
import Utilities.WrapperFunctions;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseConfig extends CentralObjects{
    public AndroidDriver driver;
    public AppiumDriverLocalService service;
    public Commons objCommons;
    public WrapperFunctions objWrapperFunctions;
    public static Logger Objlogger = LogManager.getLogger(BaseConfig.class);

    @BeforeClass
    public void EnvironmentConfiguration() throws IOException {
        Objlogger.info("Test Started");
        Properties prop = new Properties();
        FileInputStream Configfilepath = new FileInputStream(System.getProperty("user.dir")+"//src//main//resources//Config.properties");
        prop.load(Configfilepath);
        service = new AppiumServiceBuilder().withIPAddress(prop.getProperty("ipAddress")).usingPort(Integer.parseInt(prop.getProperty("port"))).build();
        service.start();
        service.clearOutPutStreams();
        UiAutomator2Options options = new UiAutomator2Options();
        options.setDeviceName(prop.getProperty("deviceName"));
        options.setApp(System.getProperty("user.dir")+ File.separator+"src\\test\\resources"+ File.separator+prop.getProperty("ApkFileName"));
        driver = new AndroidDriver(new URL(prop.getProperty("AppiumServerURL")), options);
        this.setAndroidDriver(driver);
        this.objCommons = new Commons();
        this.setCommonsPage(objCommons);
        this.objWrapperFunctions = new WrapperFunctions();
        this.setWrapperFunctions(objWrapperFunctions);
    }

    public void switchContext(String contextName){
        System.out.println("************************************************");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(12000));
        Set<String> contexts = driver.getContextHandles();
        for(String view :contexts){
            System.out.println(view +" found");
            if(view.contains(contextName) || view.equalsIgnoreCase(contextName)){
                driver.context(view);
                break;
            }
        }
        System.out.println("Current Context "+driver.getContext());
    }

    @AfterClass
    public void tearDownEnvironment(){
        driver.quit();
        service.stop();
    }
}