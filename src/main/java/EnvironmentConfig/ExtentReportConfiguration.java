package EnvironmentConfig;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportConfiguration {
    static ExtentReports extent;

    public static ExtentReports getReportObject(){
        String setReportPath = System.getProperty("user.dir")+"\\reports\\TestReport.html";
        ExtentSparkReporter reporter = new ExtentSparkReporter(setReportPath);
        reporter.config().setReportName("ExtentReports");
        reporter.config().setDocumentTitle("Test Reports");
        reporter.config().setTheme(Theme.DARK);

        extent = new ExtentReports();
        extent.attachReporter(reporter);
        extent.setSystemInfo("Author","Ashish Patil");
        return extent;
    }
}
