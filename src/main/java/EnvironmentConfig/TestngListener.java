package EnvironmentConfig;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestngListener extends CentralObjects implements ITestListener {
    ExtentReports extent = ExtentReportConfiguration.getReportObject();
    static ExtentTest test;

    @Override
    public void onTestStart(ITestResult result) {
        test = extent.createTest(result.getMethod().getMethodName()+"Test Initialized");
        this.setObjExtentTest(test);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        test.log(Status.PASS,"Test Passed Succssfully");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        test.fail("Failed because of - "+result.getThrowable());
        try {
            String path = CentralObjects.getCommonsPage().getScreenshotPath(result.getMethod().getMethodName(),CentralObjects.getAndroidDriver());
            test.addScreenCaptureFromPath(path,"Screenshot");
            String screenshot = ((TakesScreenshot) CentralObjects.getAndroidDriver()).getScreenshotAs(OutputType.BASE64);
            LoggingUtils.logBase64(screenshot, "Issue Found on page");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        test.info("Skipped because of - "+ result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {
        test.log(Status.PASS,"Execution Ended");
        extent.flush();
    }
}