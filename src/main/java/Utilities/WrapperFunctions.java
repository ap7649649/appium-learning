package Utilities;
import EnvironmentConfig.CentralObjects;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.*;
import java.util.NoSuchElementException;

public class WrapperFunctions {
//    CentralObjects objectCentral;
    private final AndroidDriver driver = CentralObjects.getAndroidDriver();
    private boolean flag;
//    public WrapperFunctions(CentralObjects objects){
//        this.objectCentral =  objects;
//        driver = CentralObjects.getAndroidDriver();
//    }

    public void longClickGesture(WebElement element){
        driver.executeScript("mobile: longClickGesture",
                ImmutableMap.of("elementId", ((RemoteWebElement) element).getId(),"duration",3000));
    }

    public boolean UiAutomatorScroll(String ElementName){
        flag = false;
        try{
            String Script = "new UiScrollable(new UiSelector()).scrollIntoView(text(\""+ElementName+"\""+"));";
            driver.findElement(AppiumBy.androidUIAutomator(Script));
            flag = true;
        }catch (Exception e){
            System.out.println("Intended WebElement not Found");
        }
        return flag;
    }

    public void ScrollToEnd(){
        boolean canScrollMore;
        do{
            canScrollMore = (Boolean) ((JavascriptExecutor) driver).executeScript("mobile: scrollGesture", ImmutableMap.of(
                    "left", 100,
                    "top", 100,
                    "width", 200,
                    "height", 200,
                    "direction", "down",
                    "percent", 3.0)
            );
        }while(canScrollMore);
    }

    /*
    Scroll Displayed Element Upwards
    Works only if Provided Element is displayed on Screen.
    */
    public void ScrollUsingW3CActions(WebElement element){
        int centerX = element.getRect().x + (element.getSize().width/2);
        double startY = element.getRect().y + (element.getSize().height * 0.9);
        double endY = element.getRect().y + (element.getSize().height * 0.1);
        //Type of Pointer Input
        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH,"finger");
        //Creating Sequence object to add actions
        Sequence swipe = new Sequence(finger,1);
        //Move finger into starting position
        swipe.addAction(finger.createPointerMove(Duration.ofSeconds(0),PointerInput.Origin.viewport(),centerX,(int)startY));
        //Finger comes down into contact with screen
        swipe.addAction(finger.createPointerDown(0));
        //Finger moves to end position
        swipe.addAction(finger.createPointerMove(Duration.ofMillis(700),PointerInput.Origin.viewport(),centerX, (int)endY));
        //Get up Finger from Screen
        swipe.addAction(finger.createPointerUp(0));
        //Perform the actions
        driver.perform(Arrays.asList(swipe));
    }

    /*
     Image than needs to be swiped should have focusable attribute to be true.
    */
    public void SwipeInSpecificDirection(WebElement element,String direction,double percent){
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture",
                ImmutableMap.of("elementId", ((RemoteWebElement)element).getId(),
                        "direction", direction,
                        "percent", percent));
    }

    public void dragElementUsingGestures(WebElement element,int DropXCoordinates,int DropYCoordinates){
        ((JavascriptExecutor) driver).executeScript("mobile: dragGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) element).getId(),
                "endX", DropXCoordinates,
                "endY", DropYCoordinates
        ));
    }

    public void rotateDeviceLandscape(String mode){
        if(mode.equalsIgnoreCase("Portrait")){
            driver.rotate(ScreenOrientation.PORTRAIT);
        }else if(mode.equalsIgnoreCase("Landscape")){
            driver.rotate(ScreenOrientation.LANDSCAPE);
        }else{
            throw new InvalidArgumentException("Pls provide proper orientation mode");
        }
    }
    public void setTextToClipBoard(String text){
        driver.setClipboardText(text);
    }
    public String getTextToClipBoard(){
        return driver.getClipboardText();
    }

    /*
    presAndroidKey
    Use : Press Specific Andoid keys
    Arguments : KeyName
     */
    public  void presAndroidKey(String key){
        if(key.equalsIgnoreCase(("Home"))){
            driver.pressKey(new KeyEvent(AndroidKey.HOME));
        }else if(key.equalsIgnoreCase(("Back"))){
            driver.pressKey(new KeyEvent(AndroidKey.BACK));
        }else if(key.equalsIgnoreCase(("Enter"))){
            driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        }else if(key.equalsIgnoreCase(("Clear"))) {
            driver.pressKey(new KeyEvent(AndroidKey.DEL));
        }else{
            throw new InvalidArgumentException("Pls provide proper KeyName");
        }
    }
    /*
    Command To get Activity name in Command Prompt.
    CMD Command --> adb shell dumpsys window | find "mCurrentFocus" (This captures Activityname)
    */
    public  void JumpToSpecificPageUsingActivityName(String AppPackageName,String ActivityName){
        Activity activity = new Activity(AppPackageName,ActivityName);
        driver.startActivity(activity);
    }

    public boolean checkElementDisplayed(WebElement element){
        flag = false;
        try {
            flag = element.isDisplayed();
            flag = true;
            return flag;
        } catch (Exception e) {
            return flag;
        }
    }

    public boolean checkElementSelected(WebElement element){
        checkElementDisplayed(element);
        return element.isSelected();
    }

    public boolean checkElementEnabled(WebElement element){
        return element.isEnabled();
    }

    public void implicitlyWaitFor(int seconds){
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
    }

    public void waitTillElementLoads(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    public void waitUntitElementDisappears(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(5));
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public boolean click(WebElement element) {
        flag = false;
        try{
            waitTillElementLoads(element);
            try {
                element.click();
                flag=true;
            }catch(Exception e){
                System.out.println(e.getMessage());
                return flag;
            }
            return flag;
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return flag;
        }
    }
}