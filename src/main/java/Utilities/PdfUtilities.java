package Utilities;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class PdfUtilities {
    private PDDocument document;
    private Calendar date;
    private PDDocumentInformation pdd;
    private PDPage page;
    private boolean flag = false;
    public void loadExistingPDF(String path) {
        try {
            File file = new File(path);
            document = PDDocument.load(file);
            flag = true;
        } catch (Exception e) {
            flag = false;
            System.out.println("Unable to load PDF file");
            e.printStackTrace();
        }
    }

    public void createEmptyPDF() {
        document = new PDDocument();
    }

    public void saveCreatedPdfFile(String filename, String path) throws IOException {
        if (!(path == "")) {
            document.save(path + filename);
        } else {
            document.save(System.getProperty("user.dir") + "//pdfFolder//" + filename);
        }
    }

    public void addBlankPagesToExistingPDF(int count) {
        for (int i = 0; i < count; i++) {
            PDPage blankPage = new PDPage();
            document.addPage(blankPage);
        }
    }

    public void removePageFromPdf(int index) {
        document.removePage(index);
    }

    public void closePDF() throws IOException {
        document.close();
    }

    public int getPdfPageCount() {
        int noOfPages = document.getNumberOfPages();
        return noOfPages;
    }

    public void setDocumentProperties(String Author, String DocumentTitle, String Creator, String subject, List<String> creationDate, List<String> modifiedDate, String... keywords) {
        pdd = document.getDocumentInformation();
        if (!(Author == "")) {
            pdd.setAuthor(Author);
        }
        if (!(DocumentTitle == "")) {
            pdd.setTitle(DocumentTitle);
        }
        if (!(Creator == "")) {
            pdd.setCreator(Creator);
        }
        if (!(subject == "")) {
            pdd.setSubject(subject);
        }
        if (!(keywords.length < 0)) {
            for (String key : keywords) {
                pdd.setKeywords(key);
            }
        }
        if (!(creationDate.size() < 0)) {
            date = new GregorianCalendar();
            date.set(Integer.parseInt(creationDate.get(0)), Integer.parseInt(creationDate.get(1)), Integer.parseInt(creationDate.get(2)));
            pdd.setCreationDate(date);
        }
        if (!(modifiedDate.size() < 0)) {
            date = new GregorianCalendar();
            date.set(Integer.parseInt(modifiedDate.get(0)), Integer.parseInt(modifiedDate.get(1)), Integer.parseInt(modifiedDate.get(2)));
            pdd.setModificationDate(date);
        }
    }

    public void printAllDocumentProperties() {
        pdd = document.getDocumentInformation();
        System.out.println("Author of the document is :" + pdd.getAuthor());
        System.out.println("Title of the document is :" + pdd.getTitle());
        System.out.println("Subject of the document is :" + pdd.getSubject());
        System.out.println("Creator of the document is :" + pdd.getCreator());
        System.out.println("Creation date of the document is :" + pdd.getCreationDate());
        System.out.println("Modification date of the document is :" + pdd.getModificationDate());
        System.out.println("Keywords of the document are :" + pdd.getKeywords());
    }

    public void addTextToPage(String index, List<String> multilineText) throws IOException {
        page = document.getPage(Integer.parseInt(index));
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.beginText();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream.newLineAtOffset(50, 600);
        contentStream.setLeading(14.5f);
        for (String value : multilineText) {
            contentStream.showText(value);
            contentStream.newLine();
        }
        contentStream.endText();
        contentStream.close();
    }

    public String getPdfPageText() throws IOException {
        PDFTextStripper pdfStripper = new PDFTextStripper();
        String pageText = pdfStripper.getText(document);
        return pageText;
    }

    public void addImageToPage(String index, String imagePath, String imageLength, String imageBreadth) throws IOException {
        page = document.getPage(Integer.parseInt(index));
        PDImageXObject pdImage = PDImageXObject.createFromFile(imagePath, document);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.drawImage(pdImage, Float.parseFloat(imageLength), Float.parseFloat(imageBreadth));
        contentStream.close();
    }

    public void encryptPdfFile(String ownerPasskey, String userPasskey) {
        boolean isEncrypted = false;
        try {
            AccessPermission accessPermission = new AccessPermission();
            StandardProtectionPolicy spp = new StandardProtectionPolicy(ownerPasskey, userPasskey, accessPermission);
            spp.setEncryptionKeyLength(128);
            spp.setPermissions(accessPermission);
            document.protect(spp);
            isEncrypted = true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (isEncrypted) {
                System.out.println("Document Encrypted Success");
            } else {
                System.out.println("Document Encryption failed");
            }
        }
    }
}
