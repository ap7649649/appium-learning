package Utilities;

import EnvironmentConfig.BaseConfig;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.OutputType;
import java.io.File;
import java.io.IOException;


public class Commons extends BaseConfig{

    public void presAndroidKey(String key){
        if(key.equalsIgnoreCase(("Home"))){
            driver.pressKey(new KeyEvent(AndroidKey.HOME));
        }else if(key.equalsIgnoreCase(("Back"))){
            driver.pressKey(new KeyEvent(AndroidKey.BACK));
        }else if(key.equalsIgnoreCase(("Enter"))){
            driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        }else if(key.equalsIgnoreCase(("Clear"))) {
            driver.pressKey(new KeyEvent(AndroidKey.DEL));
        }else{
            throw new InvalidArgumentException("Pls provide proper KeyName");
        }
    }
    public String getScreenshotPath(String testcaseName, AndroidDriver driver) throws IOException {
        File sourceFile = driver.getScreenshotAs(OutputType.FILE);
        String destFile = System.getProperty("user.dir")+"//reports"+testcaseName+".png";
        FileUtils.copyFile(sourceFile,new File(destFile));
        return destFile;
    }
}