package Pages;
import EnvironmentConfig.BaseConfig;
import EnvironmentConfig.CentralObjects;
import Utilities.WrapperFunctions;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.epam.reportportal.annotations.Step;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public class SignInPage {
    WrapperFunctions objectWrapper;
    AndroidDriver driver = CentralObjects.getAndroidDriver();
    ExtentTest test = CentralObjects.getObjExtentTest();

    public  SignInPage(){
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        this.objectWrapper = CentralObjects.getObjWrapperFunctions();
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Select the country where you want to shop']")
    private WebElement SelectCountryMessage;

    @AndroidFindBy(id="com.androidsample.generalstore:id/nameField")
    private WebElement nameInput;

    @AndroidFindBy(id = "com.androidsample.generalstore:id/spinnerCountry")
    private WebElement countrySelectButton;

    @AndroidFindBy(id = "com.androidsample.generalstore:id/btnLetsShop")
    private WebElement submitButton;

    public void  checkSelectCountryMessageDisplayed(){
        objectWrapper.checkElementDisplayed(SelectCountryMessage);
        BaseConfig.Objlogger.info("Verify Select Country Message Displayed");
    }

    @Step
    public void  setNameField(String name){
        test.log(Status.PASS,"Send Name to Name Field");
        BaseConfig.Objlogger.info("Send Name "+name+" to Field");
        nameInput.sendKeys(name);
        driver.hideKeyboard();
        BaseConfig.Objlogger.debug("Main page displayed");
    }
    @Step
    public void setGender(String gender){
        WebElement genderCheckBox = driver.findElement(AppiumBy.xpath("//android.widget.RadioButton[@text='"+gender+"']"));
        objectWrapper.waitTillElementLoads(genderCheckBox);
        objectWrapper.click(genderCheckBox);
        BaseConfig.Objlogger.info("Set Gender as "+gender);
    }
    @Step
    public void setCountry(String country){
        objectWrapper.waitTillElementLoads(countrySelectButton);
        countrySelectButton.click();
        objectWrapper.implicitlyWaitFor(3);
        objectWrapper.UiAutomatorScroll(country);
        WebElement CountryOption = driver.findElement(AppiumBy.xpath("//android.widget.TextView[@text='"+country+"']"));
        objectWrapper.checkElementDisplayed(CountryOption);
        objectWrapper.click(CountryOption);
        BaseConfig.Objlogger.info("Set Country as "+country);
    }
    @Step
    public void clickLetsGoButton(){
        objectWrapper.checkElementDisplayed(submitButton);
        objectWrapper.click(submitButton);
        objectWrapper.waitUntitElementDisappears(submitButton);
        BaseConfig.Objlogger.info("Click Let's Go Button");
    }

    public void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * endPercentage);
        new TouchAction(driver)
                .press(point(anchor, startPoint))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(anchor, endPoint))
                .release().perform();
    }
}