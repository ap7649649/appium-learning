package Pages;

import Utilities.WrapperFunctions;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class WebViewChromePage extends WrapperFunctions {
    AndroidDriver driver;
    public WebViewChromePage(){
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath = "//input[@name='q']")
    private WebElement searchInput;

    public void searchForText(String searchText){
        implicitlyWaitFor(6000);
        searchInput.sendKeys(searchText);
        searchInput.sendKeys(Keys.ENTER);
        waitUntitElementDisappears(searchInput);
    }

    public void verifyTextSearched(String text){
        boolean flag = checkElementDisplayed(driver.findElement(By.xpath("//h3[text()='"+text+"']")));
        Assert.assertTrue(flag);
    }
}
