package Pages;

import EnvironmentConfig.CentralObjects;
import Utilities.WrapperFunctions;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CheckOutPage extends WrapperFunctions {
    AndroidDriver driver = CentralObjects.getAndroidDriver();
    public CheckOutPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(id = "com.androidsample.generalstore:id/totalAmountLbl")
    private WebElement CartFinalPrice;

    @AndroidFindBy(id = "com.androidsample.generalstore:id/termsButton")
    private WebElement TermsAndCondition;

    @AndroidFindBy(id = "com.androidsample.generalstore:id/alertTitle")
    private WebElement TermsPopupLabel;

    @AndroidFindBy(id = "android:id/button1")
    private WebElement TermsPopupLabelClose;

    @AndroidFindBy(className = "android.widget.CheckBox")
    private WebElement PromotionCheckBox;

    @AndroidFindBy(id = "com.androidsample.generalstore:id/btnProceed")
    private WebElement WebProceedButton;

    public void verifyCartFinalAmount(String finalAmount) {
        String DisplayedPrice = CartFinalPrice.getText().trim();
        Assert.assertTrue(DisplayedPrice.contains((finalAmount)));
    }
    public void verifyCartPageAddedProductPrice(String ProductName, String amount) {
        WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@text='" + ProductName + "']/following-sibling::android.widget.LinearLayout/android.widget.TextView[1]"));
        waitTillElementLoads(element);
        String price = element.getText().trim();
        Assert.assertEquals(amount, price);
    }
    public void clickTermsAndConditionLabel() {
        longClickGesture(TermsAndCondition);
    }
    public void verifyTermsPopupDisplayed() {
        String label = TermsPopupLabel.getText().trim();
        Assert.assertEquals(label, "Terms Of Conditions");
    }
    public void closeTermsPopup() {
        click(TermsPopupLabelClose);
    }
    public void clickAndCheckPromotionEmailCheckboxSelected() {
        click(PromotionCheckBox);
        implicitlyWaitFor(1);
        String isChecked = PromotionCheckBox.getAttribute("checked");
        Assert.assertTrue(Boolean.parseBoolean(isChecked));
    }
    public void clickWebsiteProceedButton() {
        click(WebProceedButton);
    }
}