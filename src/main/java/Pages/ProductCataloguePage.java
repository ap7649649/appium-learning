package Pages;

import EnvironmentConfig.CentralObjects;
import Utilities.WrapperFunctions;
import org.testng.Assert;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ProductCataloguePage extends WrapperFunctions {
    AndroidDriver driver = CentralObjects.getAndroidDriver();
    public ProductCataloguePage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(id = "com.androidsample.generalstore:id/appbar_btn_cart")
    private WebElement CartIcon;

    @AndroidFindBy(xpath = "//*[(@resource-id='com.androidsample.generalstore:id/appbar_btn_cart')]/following-sibling::android.widget.TextView")
    private WebElement AddedItemCount;

    public void AddProductToCart(String ProductName) {
        WebElement addToCartButton = driver.findElement(By.xpath("//android.widget.TextView[@text='"+ProductName+"']/following-sibling::android.widget.LinearLayout/android.widget.TextView[2]"));
        UiAutomatorScroll(ProductName);
        if(checkElementDisplayed(addToCartButton)){
            click(addToCartButton);
        }else{
            System.out.println("No Such Product found in Catalogue");
        }
    }

    public String getProductPrice(String ProductName){
        WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@text='"+ProductName+"']/following-sibling::android.widget.LinearLayout/android.widget.TextView[1]"));
        String price = element.getText();
        return price;
    }

//    public void getMobileElementList(WebElement element){
//        ArrayList productList = new ArrayList();
//        SwipeInSpecificDirection(element,"down",0.2);
//    }
    public void verifyProductAdded(String ProductName){
        String addToCartButton = driver.findElement(By.xpath("//android.widget.TextView[@text='"+ProductName+"']/following-sibling::android.widget.LinearLayout/android.widget.TextView[2]")).getText();
        Assert.assertTrue(addToCartButton.contains("ADDED"));
    }

    public void clickCartIcon(){
        click(CartIcon);
    }

    public void verifyCartAddedItemCount(String itemCount){
        String count = AddedItemCount.getText().trim();
        Assert.assertEquals(count,itemCount);
    }
}