package Assignments;

import EnvironmentConfig.BaseConfig;
import Pages.CheckOutPage;
import Pages.ProductCataloguePage;
import Pages.SignInPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAssignment2 extends BaseConfig {
    @Test
    public void Assignment() throws InterruptedException {
        Objlogger.info("Testing Started");
        SignInPage objSignInPage = new SignInPage();
        ProductCataloguePage objProductCataloguePage = new ProductCataloguePage();
        CheckOutPage objCheckOutPage = new CheckOutPage();
        Thread.sleep(10000);
        objSignInPage.checkSelectCountryMessageDisplayed();
        objSignInPage.setCountry("Japan");
        objSignInPage.setNameField("Ashish");
        objSignInPage.setGender("Male");
        objSignInPage.clickLetsGoButton();
        Thread.sleep(10000);
        objProductCataloguePage.AddProductToCart("Air Jordan 4 Retro");
        String productprice = objProductCataloguePage.getProductPrice("Air Jordan 4 Retro").trim();
        Assert.assertEquals("$160.97",productprice);
        objProductCataloguePage.verifyProductAdded("Air Jordan 4 Retro");
        objProductCataloguePage.verifyCartAddedItemCount("1");
        objProductCataloguePage.clickCartIcon();
        Thread.sleep(5000);
        objCheckOutPage.verifyCartPageAddedProductPrice("Air Jordan 4 Retro",productprice);
        objCheckOutPage.verifyCartFinalAmount("160.97");
        objCheckOutPage.clickTermsAndConditionLabel();
        objCheckOutPage.verifyTermsPopupDisplayed();
        objCheckOutPage.closeTermsPopup();
        objCheckOutPage.clickAndCheckPromotionEmailCheckboxSelected();
        objCheckOutPage.clickWebsiteProceedButton();
        Objlogger.info("Testing Ended");
    }
}