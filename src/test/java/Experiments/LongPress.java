package Experiments;
// https://github.com/appium/appium-uiautomator2-driver/blob/master/docs/android-mobile-gestures.md
import EnvironmentConfig.BaseConfig;
import Utilities.WrapperFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.Assert;
import io.appium.java_client.AppiumBy;

public class LongPress extends BaseConfig {
    WrapperFunctions objWrapperFunctions = new WrapperFunctions();
    @Test
    public void LongClickGestures() throws InterruptedException{
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[@content-desc='Expandable Lists']")).click();
        Thread.sleep(4000);
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Custom Adapter')]")).click();
        //Get WebElement on which to Perform LongClick
        WebElement peopleNames =  driver.findElement(By.xpath("//android.widget.TextView[@text='People Names']"));
        objWrapperFunctions.longClickGesture(peopleNames);
        Thread.sleep(2000);
        String menuText = driver.findElement(By.xpath("//android.widget.TextView[@text='Sample menu']")).getText();
        Assert.assertEquals(menuText,"Sample menu");
        Assert.assertTrue(driver.findElement(By.xpath("//android.widget.TextView[@text='Sample menu']")).isDisplayed());
    }
}