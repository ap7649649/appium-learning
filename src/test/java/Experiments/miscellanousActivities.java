package Experiments;

import EnvironmentConfig.BaseConfig;
import Utilities.WrapperFunctions;
import io.appium.java_client.AppiumBy;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class miscellanousActivities extends BaseConfig {
    WrapperFunctions objWrapperFunctions = new WrapperFunctions();

    @Test
    public void DeviceOrientation() {
        driver.findElement(AppiumBy.accessibilityId("Preference")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Preference dependencies')]")).click();
        objWrapperFunctions.rotateDeviceLandscape("LandScape");
        driver.findElement(By.id("android:id/checkbox")).click();
        driver.findElement(By.xpath("(//android.widget.RelativeLayout)[2]")).click();
        String AlertPopupTitle = driver.findElement(By.id("android:id/alertTitle")).getText();
        Assert.assertEquals(AlertPopupTitle,"WiFi settings");
        driver.findElement(By.id("android:id/edit")).sendKeys("Ashish Demo");
        driver.findElement(By.id("android:id/button1")).click();
        objWrapperFunctions.rotateDeviceLandscape("Portrait");
    }

    @Test
    public void clipBoardOperations() {
        objWrapperFunctions.setTextToClipBoard("Ashish");
        driver.findElement(AppiumBy.accessibilityId("Preference")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Preference dependencies')]")).click();
        driver.findElement(By.id("android:id/checkbox")).click();
        driver.findElement(By.xpath("(//android.widget.RelativeLayout)[2]")).click();
        driver.findElement(By.id("android:id/edit")).sendKeys(objWrapperFunctions.getTextToClipBoard());
        driver.findElement(By.id("android:id/button1")).click();
    }
    @Test
    public void pressSpecificAndroidKey() {
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        objWrapperFunctions.presAndroidKey("Back");
        driver.findElement(AppiumBy.accessibilityId("Preference")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Preference dependencies')]")).click();
        driver.findElement(By.id("android:id/checkbox")).click();
        driver.findElement(By.xpath("(//android.widget.RelativeLayout)[2]")).click();
        driver.findElement(By.id("android:id/edit")).sendKeys("Ashish");
        objWrapperFunctions.presAndroidKey("Enter");
        driver.findElement(By.id("android:id/edit")).sendKeys("Patil");
        driver.findElement(By.id("android:id/button1")).click();
    }
    @Test
    public  void JumpToSpecificPageDirectly(){
        objWrapperFunctions.JumpToSpecificPageUsingActivityName("io.appium.android.apis","io.appium.android.apis.preference.PreferenceDependencies");
        driver.findElement(By.id("android:id/checkbox")).click();
        driver.findElement(By.xpath("(//android.widget.RelativeLayout)[2]")).click();
        driver.findElement(By.id("android:id/edit")).sendKeys("Ashish");
        driver.findElement(By.id("android:id/button1")).click();
    }
}