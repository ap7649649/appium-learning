package Experiments;

import EnvironmentConfig.BaseConfig;
import Utilities.WrapperFunctions;
import io.appium.java_client.AppiumBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
public class DragActions extends BaseConfig {
    WrapperFunctions objWrapperFunctions = new WrapperFunctions();

    @Test
    public void DragDemo() throws InterruptedException {

        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        driver.findElement(AppiumBy.accessibilityId("Drag and Drop")).click();
        WebElement sourceElement = driver.findElement(AppiumBy.id("io.appium.android.apis:id/drag_dot_1"));
        objWrapperFunctions.dragElementUsingGestures(sourceElement,619,560);
        Thread.sleep((2000));
        Assert.assertEquals(driver.findElement(By.id("io.appium.android.apis:id/drag_result_text")).getText(),"Dropped!");
    }
}