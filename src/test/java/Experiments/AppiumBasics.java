package Experiments;

import EnvironmentConfig.BaseConfig;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumBy;

public class AppiumBasics extends BaseConfig {
    @Test
    public void AppiumTest(){
        driver.findElement(AppiumBy.accessibilityId("Preference")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Preference dependencies')]")).click();
        driver.findElement(By.id("android:id/checkbox")).click();
        driver.findElement(By.xpath("(//android.widget.RelativeLayout)[2]")).click();
        String AlertPopupTitle = driver.findElement(By.id("android:id/alertTitle")).getText();
        System.out.println(AlertPopupTitle);
        driver.findElement(By.id("android:id/edit")).sendKeys("Ashish Demo");
        driver.findElement(By.id("android:id/button1")).click();
    }
}