package Experiments;

import EnvironmentConfig.BaseConfig;
import Utilities.WrapperFunctions;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import io.appium.java_client.AppiumBy;

public class ScrollAction extends BaseConfig {
    WrapperFunctions objWrapperFunctions = new WrapperFunctions();
    
    @Test
    public void ScrollTestUsingAppiumCoordinates(){
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        objWrapperFunctions.ScrollToEnd();
    }

    @Test
    public void ScrollTestUsingAndroidUIAutomator(){
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        objWrapperFunctions.UiAutomatorScroll("WebView");
    }

    @Test
    public void ScrollUsingW3CActions() {
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        WebElement element = driver.findElement(AppiumBy.accessibilityId("Gallery"));
        objWrapperFunctions.ScrollUsingW3CActions(element);
    }
}