package Experiments;

import EnvironmentConfig.BaseConfig;
import Utilities.WrapperFunctions;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.appium.java_client.AppiumBy;

public class SwipeActionDemo extends BaseConfig {
    WrapperFunctions objWrapperFunctions = new WrapperFunctions();
    @Test
    public void SwipeAction() throws InterruptedException{
        driver.findElement(AppiumBy.accessibilityId("Views")).click();
        Thread.sleep(2000);
        driver.findElement(AppiumBy.accessibilityId("Gallery")).click();
        driver.findElement(AppiumBy.xpath("//android.widget.TextView[contains(@content-desc,'Photos')]")).click();
        WebElement Image1 = driver.findElement(AppiumBy.xpath("//android.widget.ImageView[1]"));
        Thread.sleep(2000);
        Assert.assertEquals(driver.findElement(AppiumBy.xpath("//android.widget.ImageView[1]")).getAttribute("focusable"),"true");
        objWrapperFunctions.SwipeInSpecificDirection(Image1,"left",0.2);
        Assert.assertEquals(driver.findElement(AppiumBy.xpath("//android.widget.ImageView[1]")).getAttribute("focusable"),"false");
    }
}